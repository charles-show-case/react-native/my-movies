import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { reducers } from './src/redux/Redux';
import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './src/pages/Home/HomePage';

const store = createStore(reducers);
const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen
  }
});
const AppContainer = createAppContainer(AppNavigator);

export default function App() {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  );
}
