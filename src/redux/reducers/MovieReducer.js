
import { GET_MOVIES, GET_MOVIES_SUCCESS } from './../types/MovieTypes';

const INITIAL_STATE = {
    loading: true,
    movies: [
    ],
    title: 'My Home Screen'
};

export const movieReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_MOVIES: 
            return state;
        case GET_MOVIES_SUCCESS:
            return {
                ...state,
                movies: state.movies.concat([action.movie]),
                loading: false
            }
        default:
            return state
    }
};