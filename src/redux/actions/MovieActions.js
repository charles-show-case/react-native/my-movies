import { GET_MOVIES, GET_MOVIES_SUCCESS } from './../types/MovieTypes'

export const addMovie = movie => ({
    type: GET_MOVIES_SUCCESS,
    movie: movie
  });