import { combineReducers } from 'redux';
import { movieReducer } from './reducers/MovieReducer';

export const reducers = combineReducers({
    moviereducer: movieReducer,
});