import React from 'react';
import { FlatList, Text, View, Button, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { addMovie } from './../../redux/actions/MovieActions';

class HomeScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: "center" }}>
                <Text>{this.props.title}</Text>
                <Button
                    onPress={() => {
                        this.props.addMovie('test');
                    }}
                    title={'Add'}
                />
                <FlatList
                    data={this.props.movies}
                    keyExtractor={(i, index) => index.toString()}
                    renderItem={({ item }) => (
                        <Text>{item}</Text>
                    )}
                />
                
            </View>
        );
    }
}

const mapStateToProps = store => {
    return {
        ...store.moviereducer
    }
};
const mapDispatchToProps = dispatch => {
    return {
        addMovie: (movie) => {
            dispatch(addMovie(movie));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);